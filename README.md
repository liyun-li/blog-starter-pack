## 🚀 Quick start

Start using this starter pack:

```shell
gatsby new BLOG_NAME https://gitlab.com/liyun-li/blog-starter-pack
```

Pretty quick huh?

## Blogging

Take a look at `siteMetadata` in `gatsby-config.js` to change header and footer.

Visit `src/routes.ts` to add more links to the navigation bar.

Starting adding posts in `posts`!

## Testing

```shell
npm run develop
```