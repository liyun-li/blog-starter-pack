import { Box, Container, Grid, makeStyles, Typography } from '@material-ui/core'
import { graphql, useStaticQuery } from 'gatsby'
import React from 'react'
import { AppRoute } from '../components/HOC'
import { onMobile } from '../components/hooks'
import Layout from '../layouts/AppLayout'
import { Post } from '../layouts/PostLayout'

const query = graphql`
  query {
    site {
      siteMetadata {
        description
      }
    }
    allMdx(sort: { fields: [frontmatter___date], order: DESC }) {
      nodes {
        frontmatter {
          title
          author
          date(formatString: "MMMM Do, YYYY")
          featuredImageCaption
          featuredImage {
						childImageSharp {
              gatsbyImageData
            }
          }
        }
        slug
      }
    }
  }
`

const useStyles = makeStyles(theme => ({
  description: {
    [theme.breakpoints.up('md')]: {
      height: '20vw'
    },
    [theme.breakpoints.down('sm')]: {
      height: '40vw'
    },
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center'
  }
}))

const App = () => {
  const data = useStaticQuery(query)
  const posts: Post[] = data.allMdx.nodes
  const description: string = data.site.siteMetadata.description
  const classes = useStyles()
  const mobile = onMobile()
  return (
    <Layout noMargin seo='Home'>
      <Container maxWidth='lg'>
        <Box className={classes.description}>
          <Typography variant='h4'>
            <b>{description}</b>
          </Typography>
        </Box>
        {posts.map((post, index) => {
          const { frontmatter, slug } = post
          const { title, date } = frontmatter
          return (
            <Box margin='0 0 3rem 0'>
              <AppRoute to={`/${slug}`} variant={mobile ? 'h5' : 'h4'} key={index}>
                <Grid container>
                  <Grid item xs={6}>
                    <Grid container justify='flex-end'>
                      {title} |
                  </Grid>
                  </Grid>
                  <Grid item xs={6}>
                    <Grid container justify='flex-start' alignItems='flex-start'>
                      <Typography variant={mobile ? 'body1' : 'subtitle2'}>&nbsp;{date}</Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </AppRoute>
            </Box>
          )
        })}
      </Container>
    </Layout>
  )
}

export default App