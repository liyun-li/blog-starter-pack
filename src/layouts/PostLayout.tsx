import { Container, Grid, makeStyles, Typography } from '@material-ui/core'
import { MDXProvider } from '@mdx-js/react'
import { graphql } from 'gatsby'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'
import { FileNode } from 'gatsby-plugin-image/dist/src/components/hooks'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import PrismRenderer, { defaultProps, PrismTheme } from 'prism-react-renderer'
import theme from 'prism-react-renderer/themes/vsDark'
import React, { FC } from 'react'
import { PostComponent } from '../components/HOC'
import { onMobile } from '../components/hooks'
import Layout from './AppLayout'

export interface Post {
  frontmatter: {
    title: string
    author: string
    date: string
    featuredImageCaption: string
    featuredImage: FileNode
  }
  slug: string
}

type PostLayoutProps = {
  data: {
    mdx: {
      frontmatter: Post['frontmatter']
      body: string
    }
  }
}

const useStyles = makeStyles(theme => ({
  titleGrid: {
    [theme.breakpoints.down('sm')]: {
      height: '35vw'
    },
    [theme.breakpoints.up('md')]: {
      height: '25vw'
    }
  },

  titleText: {
    textAlign: 'center'
  },

  featuredImage: {
    verticalAlign: 'middle'
  }
}))

const Paragraph: FC = ({ children }) => (
  <PostComponent mobile='body1' desktop='h6' justify>
    {children}
  </PostComponent>
)

const heading2Style = {
  margin: '2rem 0 0rem 0'
}

const Heading2: FC = ({ children }) => (
  <PostComponent bold mobile='h4' desktop='h3' style={heading2Style} justify>
    {children}
  </PostComponent>
)

export const Code = ({ children, className }) => {
  const language = className.replace(/^language-/, '')
  return (
    <PrismRenderer {...defaultProps} theme={theme as PrismTheme} code={children.trim()} language={language}>
      {({ className, style, tokens, getLineProps, getTokenProps }) => (
        <pre className={className} style={{ ...style, overflowX: 'auto', padding: '20px' }}>
          {tokens.map((line, i) => (
            <div key={i} {...getLineProps({ line, key: i })}>
              {line.map((token, key) => (
                <span key={key} {...getTokenProps({ token, key })} />
              ))}
            </div>
          ))}
        </pre>
      )}
    </PrismRenderer>
  )
}

const components: { [key: string]: FC<any> } = {
  p: Paragraph,
  h2: Heading2,
  pre: props => <div {...props} />,
  code: Code
}

const PostLayout: FC<PostLayoutProps> = ({ data }) => {
  const { title, date, featuredImage, featuredImageCaption } = data.mdx.frontmatter
  const { titleGrid, featuredImage: featuredImageStyle, titleText } = useStyles()
  const mobile: boolean = onMobile()

  return (
    <Layout>
      <Container maxWidth='md'>
        <Grid container direction='column' justify='center' alignItems='center' className={titleGrid}>
          <Typography variant={mobile ? 'h5' : 'h4'} className={titleText}><b>{title}</b></Typography>
          <Typography>{date}</Typography>
        </Grid>
        {
          featuredImage && (
            <figure style={{ width: '100%' }}>
              <Grid container justify='center' alignItems='center'>
                <GatsbyImage image={getImage(featuredImage)} alt={featuredImageCaption} className={featuredImageStyle} />
              </Grid>
              {featuredImageCaption && (
                <Grid item>
                  <Grid container justify='flex-start'>
                    <figcaption>
                      <Paragraph>{featuredImageCaption}</Paragraph>
                    </figcaption>
                  </Grid>
                </Grid>
              )}
            </figure>
          )
        }
        <MDXProvider components={components}>
          <MDXRenderer>{data.mdx.body}</MDXRenderer>
        </MDXProvider>
      </Container>
    </Layout >
  )
}

export default PostLayout

export const query = graphql`
  query GetPostData($id: String!) {
    mdx(id: { eq: $id }) {
      frontmatter {
        title
        date(formatString: "MMMM Do, YYYY")
        featuredImageCaption
        featuredImage {
          childImageSharp {
            gatsbyImageData
          }
        }
      }
      body
    }
  }
`