import { Container } from '@material-ui/core'
import { MDXProvider } from '@mdx-js/react'
import React, { FC } from 'react'
import { PostComponent } from '../components/HOC'
import Layout from './AppLayout'
import { Code } from './PostLayout'

const paragraphStyle = {
  margin: '0 0 1rem 0',
  paddingLeft: '2px',
  lineHeight: '1.5'
}

const Paragraph: FC = ({ children }) => (
  <PostComponent mobile='body1' desktop='h6' justify style={paragraphStyle}>
    {children}
  </PostComponent>
)

const LI: FC = ({ children }) => (
  <li>
    <PostComponent mobile='body1' desktop='h6'>
      {children}
    </PostComponent>
  </li>
)

const unorderedListStyle = {
  margin: '0 0 1rem 0'
}

const UnorderedList: FC = ({ children }) => (
  <ul style={unorderedListStyle}>{children}</ul>
)

const heading1Style = {
  margin: '1rem 0 0 0'
}

const Heading1: FC = ({ children }) => (
  <PostComponent bold mobile='h4' desktop='h3' style={heading1Style}>
    {children}
  </PostComponent>
)

const heading2Style = {
  margin: '2rem 0 0 0'
}

const Heading2: FC = ({ children }) => (
  <PostComponent bold mobile='h5' desktop='h4' style={heading2Style}>
    {children}
  </PostComponent>
)

const heading3Style = {
  // margin: '1rem 0 0 0'
  margin: 0
}

const Heading3: FC = ({ children }) => (
  <PostComponent bold mobile='h6' desktop='h5' style={heading3Style}>
    {children}
  </PostComponent>
)

const components: { [key: string]: FC } = {
  h1: Heading1,
  h2: Heading2,
  h3: Heading3,
  p: Paragraph,
  li: LI,
  ul: UnorderedList,
  pre: props => <div {...props} />,
  code: Code as any // no idea why this should be a fix
}

const PageLayout: FC = ({ children }) => (
  <Layout>
    <Container maxWidth='sm'>
      <MDXProvider components={components}>
        {children}
      </MDXProvider>
    </Container>
  </Layout>
)

export default PageLayout