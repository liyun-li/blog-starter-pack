import '@fontsource/rubik'
import { Box, createMuiTheme, CssBaseline, makeStyles, Theme, ThemeProvider, useMediaQuery, useTheme } from '@material-ui/core'
import { graphql, useStaticQuery } from 'gatsby'
import React, { FC } from 'react'
import Footer from '../components/Footer'
import Navigation from '../components/Navigation'
import SEO from '../components/SEO'

const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
        author
      }
    }
  }
`

const defaultTheme: Theme = createMuiTheme({
  typography: {
    fontFamily: 'Rubik'
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        figure: {
          margin: '0 0 1rem 0'
        },
        figcaption: {
          fontStyle: 'italic'
        }
      }
    }
  }
})

const useStyles = makeStyles(_theme => ({
  main: {
    marginTop: '1rem'
  }
}))

type LayoutProps = {
  theme?: Theme
  noMargin?: boolean
  seo?: string
}

const AppLayout: FC<LayoutProps> = ({ noMargin, theme, children, seo }) => {
  const { main } = useStyles()
  const { title, author } = useStaticQuery(query).site.siteMetadata

  return (
    <ThemeProvider theme={theme || defaultTheme}>
      {seo && <SEO title={seo} />}
      <CssBaseline />
      <Navigation title={title} />
      <Box className={noMargin ? '' : main}>
        {children}
      </Box>
      <Footer author={author} />
    </ThemeProvider>
  )
}

export default AppLayout