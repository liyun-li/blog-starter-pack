import { Container, Box, Grid, Typography, ContainerProps } from '@material-ui/core'
import React, { FC } from 'react'

interface FooterProps {
  author: string
  maxWidth?: ContainerProps['maxWidth']
}

const Footer: FC<FooterProps> = ({ author, maxWidth }) => (
  <Container maxWidth={maxWidth}>
    <Box margin='1.5rem 0 2rem 0'>
      <Grid container justify='center' alignItems='center'>
        <Typography>
          {author} &copy; {new Date().getFullYear()}
        </Typography>
      </Grid>
      <Grid container justify='center' alignItems='center'>
        <Typography variant='body1'>
          &#9829; <a href="https://www.gatsbyjs.com">Gatsby</a> | <a href="https://about.gitlab.com/">GitLab</a> | <a href="https://material-ui.com/">Material UI</a> &#9829;
        </Typography>
      </Grid>
    </Box>
  </Container>
)

Footer.defaultProps = {
  maxWidth: 'lg'
}

export default Footer