import React, { FC } from 'react'
import { Helmet } from 'react-helmet'
import { useStaticQuery, graphql } from 'gatsby'

const query = graphql`
  query {
    site {
      siteMetadata {
        title
        description
        author
      }
    }
  }
`

type MetaType = ConcatArray<{
  name: string
  content: any
  property?: undefined
} | {
  property: string
  content: any
  name?: undefined
}>

type SEOProps = {
  title: string
  description?: string
  lang?: string
  meta?: MetaType
}

const SEO: FC<SEOProps> = ({ description, lang, meta, title }) => {
  const { site } = useStaticQuery(query)
  const metaDescription = description || site.siteMetadata.description || ``
  const defaultTitle = site.siteMetadata?.title

  return (
    <Helmet
      htmlAttributes={{
        lang: lang || 'en',
      }}
      title={title}
      titleTemplate={defaultTitle ? `%s | ${defaultTitle}` : null}
      meta={[
        {
          name: `description`,
          content: metaDescription,
        },
        {
          property: `og:title`,
          content: title,
        },
        {
          property: `og:description`,
          content: metaDescription,
        },
        {
          property: `og:type`,
          content: `website`,
        }
      ].concat(meta || [])}
    />
  )
}

export default SEO