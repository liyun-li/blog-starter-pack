import { Link, ListItem, ListItemIcon, ListItemText, makeStyles, SvgIconTypeMap, Theme, Typography, TypographyVariant } from '@material-ui/core'
import { OverridableComponent } from '@material-ui/core/OverridableComponent'
import { Variant } from '@material-ui/core/styles/createTypography'
import { Link as Route } from 'gatsby'
import React, { FC } from 'react'
import { onMobile } from './hooks'

const useStyles = makeStyles((theme: Theme) => ({
  externalLink: {
    textDecoration: 'none',
    color: theme.palette.text.secondary
  },
  justify: {
    textAlign: 'justify'
  }
}))

type AppRouteProps = {
  to: string
  children: any
  className?: string
  variant?: TypographyVariant
}

const AppRoute: FC<AppRouteProps> = props => {
  const { externalLink } = useStyles()
  return (
    <Typography variant={props.variant || 'body2'} align='center'>
      <Route to={props.to} className={props.className || externalLink} activeStyle={{ fontWeight: 'bolder' }}>
        {props.children}
      </Route>
    </Typography>
  )
}

type AppExternalLinkProps = {
  to: string
  children: any
  className?: string
  variant?: TypographyVariant
}

const AppExternalLink: FC<AppExternalLinkProps> = props => {
  const { externalLink } = useStyles()
  return (
    <Link href={props.to} className={props.className || externalLink} target='_blank'>
      <Typography variant={props.variant} align='center'>
        {props.children}
      </Typography>
    </Link>
  )
}

type ListItemRouteProps = {
  to: string
  text: string
  icon: OverridableComponent<SvgIconTypeMap<{}, "svg">>
}

const ListItemRoute: FC<ListItemRouteProps> = props => (
  <ListItem button component={Route} to={props.to} activeStyle={{ fontWeight: 'bolder' }}>
    <ListItemIcon><props.icon /></ListItemIcon>
    <ListItemText primary={props.text} />
  </ListItem>
)

type ListItemExternalLinkProps = {
  to: string
  text: string
  icon: OverridableComponent<SvgIconTypeMap<{}, "svg">>
}

const ListItemExternalLink: FC<ListItemExternalLinkProps> = props => (
  <ListItem button component={Link} href={props.to} target='_blank'>
    <ListItemIcon><props.icon /></ListItemIcon>
    <ListItemText secondary={props.text} />
  </ListItem>
)

type PostComponentProps = {
  mobile: Variant
  desktop: Variant
  bold?: boolean
  style?: any
  justify?: boolean
}

const PostComponent: FC<PostComponentProps> = ({ children, mobile, desktop, bold, style, justify }) => {
  const isMobile = onMobile()
  const { justify: justifyText } = useStyles()
  return (
    <Typography variant={isMobile ? mobile : desktop} className={justify ? justifyText : ''} style={style}>
      {bold ? <b>{children}</b> : children}
    </Typography>
  )
}

export {
  AppExternalLink,
  AppRoute,
  ListItemRoute,
  ListItemExternalLink,
  PostComponent
}
