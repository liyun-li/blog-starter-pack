import { AppBar, Container, ContainerProps, Divider, Drawer, Grid, IconButton, List, makeStyles, Paper, Theme, Toolbar } from '@material-ui/core'
import { Close, Link, Menu } from '@material-ui/icons'
import React, { FC, useState } from 'react'
import { APP_ROUTES, EXTERNAL_LINKS } from '../routes'
import { AppExternalLink, AppRoute, ListItemExternalLink, ListItemRoute } from './HOC'
import { onMobile } from './hooks'

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  },

  drawerPaper: {
    width: '65vw',
    height: '100vh'
  },

  appBar: {
    backgroundColor: 'white'
  }
}))

interface NavigationProps {
  title: string
  maxWidth?: ContainerProps['maxWidth']
}

const DesktopNavigation: FC<NavigationProps> = ({ title }) => {
  const classes = useStyles()
  return (
    <Grid container alignItems='center'>
      <Grid item xs={4}>
        <Grid container justify='flex-start' alignItems='center' spacing={4}>
          {Object.entries(APP_ROUTES).map(([text, { to }], key) => (
            <Grid item key={key}><AppRoute to={to}>{text}</AppRoute></Grid>
          ))}
        </Grid>
      </Grid>
      <Grid item xs={4}>
        <AppRoute to='/' className={classes.title} variant='h6'>
          <b>{title}</b>
        </AppRoute>
      </Grid>
      <Grid item xs={4}>
        <Grid container justify='flex-end' alignItems='center' spacing={4}>
          {
            Object.entries(EXTERNAL_LINKS).map(([text, { to: href }], key) => (
              <Grid item key={key}>
                <AppExternalLink to={href} variant='subtitle2'>
                  {text}
                </AppExternalLink>
              </Grid>
            ))
          }
        </Grid>
      </Grid>
    </Grid>
  )
}

const MobileNavigation: FC<NavigationProps> = ({ title }) => {
  const classes = useStyles()
  const [drawerOpen, setDrawerOpen] = useState(false)

  return (
    <Grid container justify='center' alignItems='center'>
      <Drawer variant='temporary' open={drawerOpen} ModalProps={{
        keepMounted: true, // Better open performance on mobile
      }} onClose={() => setDrawerOpen(false)}>
        <Paper className={classes.drawerPaper}>
          <Toolbar>
            <Grid container alignItems='center' justify='flex-end'>
              <IconButton onClick={() => setDrawerOpen(false)} disableFocusRipple>
                <Close />
              </IconButton>
            </Grid>
          </Toolbar>
          <List>
            {
              Object.entries(APP_ROUTES).map(([text, { to, icon }], key) => (
                <ListItemRoute to={to} icon={icon || Link} text={text} key={key} />
              ))
            }
            <Divider style={{ marginTop: '0.5rem', marginBottom: '0.5rem' }} />
            {
              Object.entries(EXTERNAL_LINKS).map(([text, { to: href, icon }], key) => (
                <ListItemExternalLink to={href} icon={icon || Link} text={text} key={key} />
              ))
            }
          </List>
        </Paper>
      </Drawer>

      <Grid item xs={2}>
        <IconButton onClick={() => setDrawerOpen(true)} disableFocusRipple>
          <Menu />
        </IconButton>
      </Grid>
      <Grid item xs={8}>
        <AppRoute to='/' className={classes.title} variant='h6'>
          <b>{title}</b>
        </AppRoute>
      </Grid>
      <Grid item xs={2} />
    </Grid>
  )
}

const Navigation: FC<NavigationProps> = ({ title, maxWidth }) => {
  const { appBar } = useStyles()
  return (
    <AppBar position='relative' className={appBar}>
      <Toolbar>
        {onMobile() ? <MobileNavigation title={title} /> : (
          <Container maxWidth={maxWidth}>
            <DesktopNavigation title={title} />
          </Container>
        )}
      </Toolbar>
    </AppBar>
  )
}

Navigation.defaultProps = {
  maxWidth: 'lg'
}

export default Navigation