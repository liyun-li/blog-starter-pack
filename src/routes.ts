import { SvgIconTypeMap } from '@material-ui/core'
import { OverridableComponent } from '@material-ui/core/OverridableComponent'
import { Home, Person } from '@material-ui/icons'

interface IPath {
  [text: string]: {
    to: string
    icon?: OverridableComponent<SvgIconTypeMap<{}, "svg">>
  }
}

// note: all app routes should have a trailing slash
export const APP_ROUTES: IPath = {
  'Home': {
    to: '/',
    icon: Home
  },
  'About': {
    to: '/about/',
    icon: Person
  }
}

export const EXTERNAL_LINKS: IPath = {
  'Author': {
    to: 'https://liyun.li'
  }
}