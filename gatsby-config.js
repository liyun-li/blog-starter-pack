module.exports = {
  flags: {
    PRESERVE_WEBPACK_CACHE: true,
    PRESERVE_FILE_DOWNLOAD_CACHE: true,
    DEV_SSR: true,
    FAST_DEV: true
  },
  siteMetadata: {
    title: `Blog Starter Pack`,
    description: `Success!!`,
    author: `Liyun Li`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        defaults: {
          formats: [`auto`],
          quality: 100,
          breakpoints: [480, 720, 960, 1080, 1200, 1366, 1600, 1960]
        }
      },
    },
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,

    // material ui stuff
    `gatsby-plugin-material-ui`,

    // mdx stuff
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `posts`,
        path: `${__dirname}/posts/`,
      },
    },
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        defaultLayouts: {
          default: require.resolve(`${__dirname}/src/layouts/PageLayout.tsx`),
        },
        gatsbyRemarkPlugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 3840,
              showCaptions: true,
              quality: 100,
            },
          },
        ],
      },
    },
  ],
}
