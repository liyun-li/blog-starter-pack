const { createFilePath } = require('gatsby-source-filesystem')
const path = require('path')

const queryAllMdx = `
  query {
    allMdx {
      edges {
        node {
          id
          fields {
            slug
          }
        }
      }
    }
  }
`

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  const { data, errors } = await graphql(queryAllMdx)

  if (errors) {
    reporter.panicOnBuild('ERROR: Loading "createPages" query')
  }

  for (const { node } of data.allMdx.edges) {
    createPage({
      path: node.fields.slug,
      component: path.resolve(`./src/layouts/PostLayout.tsx`),
      context: { id: node.id },
    })
  }
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions
  if (node.internal.type === 'Mdx') {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: 'slug',
      node,
      value,
    })
  }
}